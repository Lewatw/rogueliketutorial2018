## Roguelike Dev Tutorial 2018

![RoguelikeDev Does the Roguelike Tutorial](https://i.imgur.com/EYJFgdI.png)

**Creating a roguelike game in Python 3 and libtcod**

##Table of Contents

1. [Installation](#installation)
2. [Running the game](#running the game)
3. [Contributions](#contributions)
4. [Credits](#credits)
5. [License](#license)
6. [Tools used](#tools)

---

# Installation
* Ensure the following **dependancies** are installed:
	* [Python 3.6](https://www.python.org/downloads/release/python-365/)
	* [libtcod 1.7](https://bitbucket.org/libtcod/libtcod/downloads/)

* [Clone the repo](git clone https://Lewatw@bitbucket.org/Lewatw/rogueliketutorial2018.git)

---

# Running the game

From a terminal use the command: **Python engine.py** to run the game.

**Controls**

* Movement
    * North press Up arrow or T
    * North East press y
    * East press Right arrow or H
    * South East press N
    * South  press Down arrow or B
    * South West press V
    * West press Left arrow or F
    * North West press R

* Action keys
    * Press (C) to display character information
    * Press (G) to pick up an item
    * Press (D) to drop an item
    * Press (enter) to go down the stairs
    * Press ALT + F12 to enter/leave full screen
    * Press ESCAPE to leave the current screen/menu and to exit the game back to the main menu
    * Press (I) to display your inventory
    
Press the letter to use the item, to wield/unwield a weapon press its letter

* The lightning spell will target the nearest enemy
* The fireball spell has to be targetted and will hit all targets within an area of effect
* The healing potion will heal you!

---

# Contributions
This is a personal project, used mainly to learn Python and I don't expect any external contributions.

---

# Credits
All my own work, with support from [the RoguelikeDev community](https://www.reddit.com/r/roguelikedev/), and the [roguelike tutorial](https://www.reddit.com/r/roguelikedev/wiki/python_tutorial_series).

---

# License
MIT License - you're free to use this project as you wish. You don't have to give credit, but it would be nice, however you must keep this license.

See the License for full details.

# Tools
Pycharm CE v2018.2 (or the latest version)
