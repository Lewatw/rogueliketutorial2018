import libtcodpy as libtcod

from random import randint

from game_messages import Message


class BasicMonster:
    def take_turn(self, target, fov_map, game_map, entities):
        results = []

        monster = self.owner
        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):

            if monster.distance_to(target) >= 2:
                monster.move_astar(target, entities, game_map)

            elif target.fighter.hp > 0:
                attack_results = monster.fighter.attack(target)
                results.extend(attack_results)

        return results


class SpellCasterMonster:
    def take_turn(self, target, fov_map, game_map, entities):
        results = []
        monster = self.owner
        # check if monster can see player
        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):
            y = 0
            for item in monster.inventory.items:
                if item.sub_type == 'scroll':
                    item = monster.inventory.items[y]

                    monster_used_item_results = monster.inventory.use(item, entities=entities, fov_map=fov_map)

                    for monster_used_item_result in monster_used_item_results:
                        if monster_used_item_result.get('target'):
                            results.append(
                                {'message': Message('The {0} targeted the player'.format(monster.name), libtcod.yellow)})

                        if monster_used_item_result.get('message'):
                            results.append(monster_used_item_result)

                        if monster_used_item_result.get('dead'):
                            results.append(monster_used_item_result)

                y += 1

        if monster.distance_to(target) >= 2:
            monster.move_astar(target, entities, game_map)

        elif target.fighter.hp > 0:
            attack_results = monster.fighter.attack(target)
            results.extend(attack_results)
        return results


class ConfusedMonster:
    def __init__(self, previous_ai, number_of_turns=10):
        self.previous_ai = previous_ai
        self.number_of_turns = number_of_turns

    def take_turn(self, target, fov_map, game_map, entities):
        results = []

        if self.number_of_turns > 0:
            random_x = self.owner.x + randint(0, 2) - 1
            random_y = self.owner.y + randint(0, 2) - 1

            if random_x != self.owner.x and random_y != self.owner.y:
                self.owner.move_towards(random_x, random_y, game_map, entities)

            self.number_of_turns -= 1
        else:
            self.owner.ai = self.previoous_ai
            results.append({'message': Message('The {0} is no longer confused!'.format(self.owner.name), libtcod.red)})

        return results

