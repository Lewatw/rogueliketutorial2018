class Stats:
    def __init__(self, turns_taken=0, monsters_killed=0, killed_by=None):
        self.turns_taken = turns_taken
        self.monsters_killed = monsters_killed
        self.killed_by = killed_by

#
#   Track number of actions taken by the player
#
    def increase_turn_count_by(self, value):
        self.turns_taken += value

    @property
    def get_number_of_turns_taken(self):
        return self.turns_taken

#
#   Track the number of monsters killed by the player
#
    def increase_monsters_killed_count(self):
        self.monsters_killed += 1

    @property
    def get_count_of_monsters_killed(self):
        return self.monsters_killed

#
#   Track the type of monster that killed the player
#
    def set_reason_why_player_died(self, reason):
        self.killed_by = reason

    @property
    def get_reason_for_player_death(self):
        return self.killed_by
