import libtcodpy as libtcod

from game_messages import Message

from game_states import GameStates

from render_functions import RenderOrder


def kill_player(player):
    player.char = '%'
    player.color = libtcod.dark_red

    return Message('You died!', libtcod.red), GameStates.PLAYER_DEAD


def kill_monster(monster, entities):
    monster.char = '%'
    monster.color = libtcod.dark_red
    monster.blocks = False
    monster.fighter = None
    monster.ai = None
    monster.render_order = RenderOrder.CORPSE
    death_message = Message('{0} is dead.'.format(monster.name.capitalize()), libtcod.orange)

    if monster.name == 'Demon':

        for item in monster.inventory.items:
            monster.inventory.drop_item(monster, item)
            entities.append(item)
            death_message = Message('The {0} is dead, it drops a {1}.'.format(monster.name.capitalize(), item.name),
                                    libtcod.orange)

    monster.name = 'body of ' + monster.name

    return death_message
