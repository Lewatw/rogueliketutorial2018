from enum import Enum, auto


class GameStates(Enum):
    PLAYERS_TURN = auto()
    PLAYER_WIN = auto()
    PLAYER_DEAD = auto()
    ENEMY_TURN = auto()
    SHOW_INVENTORY = auto()
    DROP_INVENTORY = auto()
    TARGETING = auto()
    LEVEL_UP = auto()
    CHARACTER_SCREEN = auto()


