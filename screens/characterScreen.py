import libtcodpy as libtcod


def character_screen(player, character_screen_width, character_screen_height, screen_width, screen_height):

    left_column = 1

    window = libtcod.console_new(character_screen_width, character_screen_height)

    libtcod.console_set_default_background(window, libtcod.light_gray)
    libtcod.console_set_default_foreground(window, libtcod.white)

    libtcod.console_clear(window)

    libtcod.console_print_rect_ex(window, left_column, 2, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Level: {0}'.format(player.level.current_level))
    libtcod.console_print_rect_ex(window, left_column, 3, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Experience: {0}'.format(player.level.current_xp))
    libtcod.console_print_rect_ex(window, left_column, 4, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Experience to Level: {0}'.format(player.level.experience_to_next_level))
    libtcod.console_print_rect_ex(window, left_column, 6, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Maximum HP: {0}'.format(player.fighter.max_hp))
    libtcod.console_print_rect_ex(window, left_column, 7, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Attack: {0}'.format(player.fighter.power))
    libtcod.console_print_rect_ex(window, left_column, 8, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Defense: {0}'.format(player.fighter.defense))

    libtcod.console_print_rect_ex(window, left_column, 13, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Press ESC to continue')

    x = screen_width // 2 - character_screen_width // 2
    y = screen_height // 2 - character_screen_height // 2

    libtcod.console_set_default_background(window, libtcod.white)
    libtcod.console_set_default_foreground(window, libtcod.blue)

    libtcod.console_print_frame(window, 0, 0, character_screen_width, character_screen_height, False,
                                libtcod.BKGND_DEFAULT, 'Character Information')

    libtcod.console_blit(window, 0, 0, character_screen_width, character_screen_height, 0, x, y, 1.0, 0.7)