import libtcodpy as libtcod


def game_over_screen(player, game_over_screen_width, game_over_screen_height, screen_width, screen_height):

    left_column = 1

    window = libtcod.console_new(game_over_screen_width, game_over_screen_height)

    libtcod.console_set_default_background(window, libtcod.lightest_red)
    libtcod.console_set_default_foreground(window, libtcod.white)

    libtcod.console_clear(window)

# start row for game over messages should be 2 - any lower and it will collide with the frame title

    libtcod.console_print_rect_ex(window, left_column, 2, game_over_screen_width, game_over_screen_height,
                                  libtcod.BKGND_NONE,
                                  libtcod.LEFT,
                                  'You were killed by {0}'.format(player.stats.get_reason_for_player_death))

    libtcod.console_print_rect_ex(window, left_column, 4, game_over_screen_width, game_over_screen_height,
                                  libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Turns taken: {0}'.format(player.stats.get_number_of_turns_taken))

    libtcod.console_print_rect_ex(window, left_column, 5, game_over_screen_width, game_over_screen_height,
                                  libtcod.BKGND_NONE,
                                  libtcod.LEFT, 'Monsters killed: {0}'.format(player.stats.get_count_of_monsters_killed))

    libtcod.console_print_rect_ex(window, left_column, 27, game_over_screen_width, game_over_screen_height,
                                  libtcod.BKGND_NONE, libtcod.LEFT, 'Press ESC for main menu')

    x = screen_width // 2 - game_over_screen_width // 2
    y = screen_height // 2 - game_over_screen_height // 2

    libtcod.console_set_default_background(window, libtcod.white)
    libtcod.console_set_default_foreground(window, libtcod.red)

    libtcod.console_print_frame(window, 0, 0, game_over_screen_width, game_over_screen_height, False,
                                libtcod.BKGND_DEFAULT, 'Game Over')

    libtcod.console_blit(window, 0, 0, game_over_screen_width, game_over_screen_height, 0, x, y, 1.0, 0.7)