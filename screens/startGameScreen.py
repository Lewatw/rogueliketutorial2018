import libtcodpy as libtcod

from menus import menu


def game_start(con, background_image, screen_width, screen_height):
    libtcod.image_blit_2x(background_image, 0, 0, 0)

    libtcod.console_set_default_foreground(0, libtcod.light_yellow)
    libtcod.console_print_ex(0, int(screen_width / 2), int(screen_height / 2) - 8, libtcod.BKGND_NONE, libtcod.CENTER,
                             'Return of the Ancient One')
    libtcod.console_print_ex(0, int(screen_width / 2), int(screen_height / 2) + 8, libtcod.BKGND_NONE, libtcod.CENTER,
                             '(c) Steven Devonport 2018')

    menu(con, '', ['Play a new game', 'Continue last game', 'Quit'], 24, screen_width, screen_height)