import libtcodpy as libtcod


def you_win_screen(you_win_screen_width, you_win_screen_height, screen_width, screen_height):

    left_column = 1

    window = libtcod.console_new(you_win_screen_width, you_win_screen_height)

    libtcod.console_set_default_background(window, libtcod.black)
    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_set_background_flag(window, libtcod.BKGND_DEFAULT)

    libtcod.console_clear(window)

    # start row for game over messages should be 2 - any lower and it will collide with the frame title

    libtcod.console_print_rect_ex(window, left_column, 27, you_win_screen_width, you_win_screen_height,
                                            libtcod.BKGND_NONE, libtcod.LEFT, 'Press ESC for main menu')

    x = screen_width // 2 - you_win_screen_width // 2
    y = screen_height // 2 - you_win_screen_height // 2

    libtcod.console_set_default_background(window, libtcod.white)
    libtcod.console_set_default_foreground(window, libtcod.red)

    libtcod.console_print_frame(window, 0, 0, you_win_screen_width, you_win_screen_height, False,
                                libtcod.BKGND_DEFAULT, 'Y O U  W I N')

    libtcod.console_blit(window, 0, 0, you_win_screen_width, you_win_screen_height, 0, x, y, 1.0, 0.7)
